package hu.unideb.inf;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;

public class Stepdefs {
    private static final int WAIT_TIME = 10;
    static HomePage homePage;
    static WebDriver driver;

    static {
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        options.setHeadless(true);
        options.addArguments("--no-sandbox");
        driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(WAIT_TIME, TimeUnit.SECONDS);
    }


    @Given("The home page is opened")
    public void theHomePageIsOpened() {
        homePage = new HomePage(driver);
    }

    @And("The Sign In link is clicked")
    public void theSignInLinkIsClicked() {
        homePage.clickSignInLink();
    }

    @Given("The Sign In button is clicked")
    public void theSignInButtonIsClicked() {
        homePage.clickSignInButton();
    }

    @Given("The {string} is filled in with {string}")
    public void theFieldIsFilledInWithValue(String field, String value) {
        homePage.fillOutField(field, value);
    }

    @Then("The {string} is shown")
    public void theMsgIsShown(String msg) {
        Optional<String> errorMessage = homePage.getErrorMessage();
        if (errorMessage.isPresent()) {
            Assert.assertEquals(msg, errorMessage.get());
        } else {
            fail();
        }
    }

    @And("the {string} is filled in with {string}")
    public void thePasswordFieldIsFilledInWithPassword(String field, String value) {
        homePage.fillOutField("passwd", value);
    }

    @And("Create an account is clicked")
    public void createAnAccountIsClicked() {
        homePage.clickCreateAccountButton();
    }

    @Then("The {string} is show")
    public void theCreateAccountErrormsgIsShow(String msg) {
        Optional<String> errorMessage = homePage.getCreateAccountErrorMessage();
        if (errorMessage.isPresent()) {
            Assert.assertEquals(msg, errorMessage.get());
        } else {
            fail();
        }
    }

    @Then("The {string} is present")
    public void theSearchErrorMessageIsPresent(String msg) {
        Optional<String> errorMessage = homePage.getSearchErrorMessage();
        if (errorMessage.isPresent()) {
            Assert.assertEquals(msg, errorMessage.get());
        } else {
            fail();
        }
    }

    @And("Search Button is clicked")
    public void searchButtonIsClicked() {
        homePage.clickSearchButton();
    }

    @Then("no error message is shown")
    public void noErrorMessageIsShown() {
        Optional<String> errorMessage = homePage.getSearchErrorMessage();
        if (errorMessage.isPresent()) {
            fail();
        } else {
            Assert.assertEquals(errorMessage, Optional.empty());
        }
    }
}
