Feature: YourLogo Sign up page

  Background:
    Given The home page is opened
    And The Sign In link is clicked

  Scenario Outline:
    Given The '<field>' is filled in with '<value>'
    And The Sign In button is clicked
    Then The '<msg>' is shown
    Examples:
      | field | value             | msg                        |
      | email | invalid.email.com | Invalid email address.     |
      | email | valid@email.com   | Password is required.      |
      | email |                   | An email address required. |

  Scenario Outline:
    Given The '<field>' is filled in with '<value>'
    And the '<passwordField>' is filled in with '<password>'
    And The Sign In button is clicked
    Then The '<msg>' is shown
    Examples:
      | field | value           | passwordField | password | msg                    |
      | email | valid@email.com | passwd        | 123      | Invalid password.      |
      | email | valid@email.com | passwd        | 12345678 | Authentication failed. |

  Scenario Outline:
    Given The '<field>' is filled in with '<value>'
    And Create an account is clicked
    Then The '<createAccountErrormsg>' is show
    Examples:
      | field        | value             | createAccountErrormsg                                                                                                |
      | email_create | invalid.email.com | Invalid email address.                                                                                               |
      | email_create | 123@gmail.com     | An account using this email address has already been registered. Please enter a valid password or request a new one. |


  Scenario Outline:
    Given The '<field>' is filled in with '<value>'
    And Search Button is clicked
    Then The '<searchErrorMessage>' is present
    Examples:
      | field            | value | searchErrorMessage                           |
      | search_query_top | no    | No results were found for your search "no"   |
      | search_query_top | this  | No results were found for your search "this" |
      | search_query_top |       | Please enter a search keyword                |

  Scenario Outline:
    Given The '<field>' is filled in with '<value>'
    And Search Button is clicked
    Then no error message is shown
    Examples:
      | field            | value   |
      | search_query_top | dresses |
      | search_query_top | shirts  |



